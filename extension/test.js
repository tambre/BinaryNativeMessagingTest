var port = null;
var success = false;
var native_message_received = false;

function append_message(text)
{
	document.getElementById("response").innerHTML += "<p>" + text + "</p>";
}

function send_message()
{
	// Send a test message with some bytes
	var msg_buf = new ArrayBuffer(4);
	var msg_view = new Uint8Array(msg_buf);
	msg_view[0] = 0x0;
	msg_view[1] = 0x1;
	msg_view[2] = 0x42;
	msg_view[3] = 0xFF;
	port.postMessage(msg_buf);
	append_message("INFO: sent message");
}

function on_disconnected()
{
	append_message("Disconnection reason: " + chrome.runtime.lastError.message);

	if (success && chrome.runtime.lastError.message == "Native host has exited.")
	{
		append_message("SUCCESS! Check the console to see if the native client also counted the test as a success.")
	}
	else if (!native_message_received)
	{
		append_message("FAILURE! No native message received before disconnection.")
	}
	else
	{
		append_message("FAILURE! Reason unknown.");
	}

	port = null;
}

function to_hex(array_buffer)
{
	var view = new Uint8Array(array_buffer);
	var bytes = "";

	view.forEach(function(element)
	{
		bytes += "0x" + element.toString(16) + " ";
	});

	return bytes;
}

function on_native_message(message)
{
	native_message_received = true;
	
	// Make sure the received message is actually an ArrayBuffer
	if (!(message instanceof ArrayBuffer))
	{
		append_message("FAILURE: Didn't receive an ArrayBuffer");
		return;
	}

	// Make sure the received ArrayBuffer's size is 4 bytes
	if (message.byteLength != 4)
	{
		append_message("FAILURE: Received ArrayBuffer's size is " + message.byteLength + ", but it should be 4");
		return;
	}

	// Test the received bytes for correctness
	var received_message = new Uint8Array(message);

	if (received_message[0] != 0x0 || received_message[1] != 0x1 || received_message[2] != 0x10 || received_message[3] != 0xFF)
	{
		append_message("FAILURE: Received bytes are wrong");
		return;
	}

	// Add the received bytes to the log
	append_message("Received bytes: " + to_hex(message));

	// If we received a message back correctly, then everything on our side succeeded
	success = true;
}

function test()
{
	// Not sure how to check if the connection fails
	port = chrome.runtime.connectNative('binary_native_messaging_test');
	port.onMessage.addListener(on_native_message);
	port.onDisconnect.addListener(on_disconnected);
	send_message();
}

document.addEventListener('DOMContentLoaded', function()
{
   test();
}, false);