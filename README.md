# Binary native messaging test suite

Requirements:
* 64-bit Windows machine with MSVC19.1
* CMake 3.8

Building (relative to this directory):
* `cd build`
* `cmake .. -G "Visual Studio 15 2017 Win64"`
* `cmake --build . --config Release` (or build the solution in Visual Studio)

Testing:
* Run *RegisterNMBTest.bat*
* Load *extension* directory as an unpacked extension
* Navigate to the extension's options and observe test results both in the browser and the console