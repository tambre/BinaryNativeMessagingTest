cmake_minimum_required(VERSION 3.8)
project(BNMTest)

# Sets some common linker and other options on the target, which we can't set using inheritance.
macro(common_target_options TARGET)
	# Use C++17
	set_property(TARGET ${TARGET} PROPERTY CXX_STANDARD 17)
	set_property(TARGET ${TARGET} PROPERTY CXX_STANDARD_REQUIRED ON)
	set_property(TARGET ${TARGET} PROPERTY CXX_EXTENSIONS OFF)

	if(MSVC)
		# Set the debugger working directory to the bin directory that we use for storing executables and runtime stuff during development.
		set_property(TARGET ${TARGET} PROPERTY VS_DEBUGGER_WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}/bin")

		# Enable incremental linking for development.
		set_property(TARGET ${TARGET} APPEND_STRING PROPERTY LINK_FLAGS_DEBUG " /INCREMENTAL")

		# Enable partial PDB generation for faster linking during development.
		set_property(TARGET ${TARGET} APPEND_STRING PROPERTY LINK_FLAGS_DEBUG " /DEBUG:FASTLINK")

		## Enable LTCG for release builds.
		# For application projects we can simply specify a linker flag.
		set_property(TARGET ${TARGET} APPEND_STRING PROPERTY LINK_FLAGS_RELEASE " /LTCG")
		set_property(TARGET ${TARGET} APPEND_STRING PROPERTY LINK_FLAGS_MINSIZEREL " /LTCG")
		set_property(TARGET ${TARGET} APPEND_STRING PROPERTY LINK_FLAGS_RELWITHDEBINFO " /LTCG")

		# For library projects we need to specify the LTCG flag in the "Librarian" section.
		set_property(TARGET ${TARGET} APPEND_STRING PROPERTY STATIC_LIBRARY_FLAGS_RELEASE " /LTCG")
		set_property(TARGET ${TARGET} APPEND_STRING PROPERTY STATIC_LIBRARY_FLAGS_MINSIZEREL " /LTCG")
		set_property(TARGET ${TARGET} APPEND_STRING PROPERTY STATIC_LIBRARY_FLAGS_RELWITHDEBINFO " /LTCG")
	endif()
endmacro()

# Check for 64-bit
if(NOT CMAKE_SIZEOF_VOID_P EQUAL 8)
	message(FATAL_ERROR "Only 64-bit is supported.")
endif()

# Set the startup project for Visual Studio.
set_property(DIRECTORY PROPERTY VS_STARTUP_PROJECT BNMTest)

add_subdirectory(BNMTest)