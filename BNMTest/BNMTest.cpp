#include <cstdarg>
#include <cstdint>
#include <memory>
#include <string>
#include <vector>

using s64 = std::int64_t;
using s32 = std::int32_t;
using s16 = std::int16_t;
using s8  = std::int8_t;
using u64 = std::uint64_t;
using u32 = std::uint32_t;
using u16 = std::uint16_t;
using u8  = std::uint8_t;

#define USING_WIN32_LEAN_AND_MEAN
#include <Windows.h>

std::string format(const std::string format, va_list args)
{
	s32 final_n;
	s32 n = static_cast<s32>(format.size() * 2);
	std::string str;
	std::unique_ptr<char[]> formatted;

	while (1)
	{
		formatted.reset(new char[n]);
		strcpy(&formatted[0], format.c_str());

		final_n = vsnprintf(&formatted[0], n, format.c_str(), args);

		if (final_n < 0 || final_n >= n)
		{
			n += abs(final_n - n + 1);
		}
		else
		{
			break;
		}
	}

	return std::string(formatted.get());
}

std::string format(const std::string format, ...)
{
	va_list args;
	va_start(args, format);
	std::string formatted = ::format(format, args);
	va_end(args);
	return formatted;
}

void print(const char* format, ...)
{
	va_list args;
	va_start(args, format);

	std::fprintf(stderr, "%s\n", ::format(format, args).c_str());

	va_end(args);
}

// Globals
HANDLE console_in;
HANDLE console_out;

// Formats the input and actually sends the message
bool send_message(std::vector<u8>& message_buffer)
{
	std::vector<char> buffer(message_buffer.size() + 4);
	DWORD characters_sent;

	// Set the first four bytes as the message size
	u32 message_size = static_cast<u32>(message_buffer.size());
	buffer[0] = message_size & 0xFF;
	buffer[1] = (message_size >> 8) & 0xFF;
	buffer[2] = (message_size >> 16) & 0xFF;
	buffer[3] = (message_size >> 24) & 0xFF;

	// Copy the message into the buffer
	memcpy(buffer.data() + 4, message_buffer.data(), message_buffer.size());

	if (!WriteFile(console_out, buffer.data(), static_cast<DWORD>(buffer.size()), &characters_sent, nullptr))
	{
		print("FAILURE: Failed to write to console (%d)", GetLastError());
		return false;
	}

	return true;
}

bool receive_message(std::vector<u8>& message)
{
	// Wait for input
	DWORD result = WaitForSingleObject(console_in, INFINITE);

	if (result != WAIT_OBJECT_0)
	{
		print("FAILURE: Failed to wait for the message. (%d)", result);
		return false;
	}

	// Read the message length
	DWORD characters_read;
	u32 message_length;

	if (!ReadFile(console_in, &message_length, 4, &characters_read, nullptr))
	{
		print("FAILURE: Failed to read the message size. (%d)", GetLastError());
		return false;
	}

	// Read the actual message
	message.resize(message_length);

	if (!ReadFile(console_in, message.data(), message_length, &characters_read, nullptr))
	{
		print("FAILURE: Failed to read the message. (%d)", GetLastError());
		return false;
	}

	return true;
}

// Sets a file handle's mode to binary and disables buffering
bool setup_console(HANDLE handle)
{
	if (!SetConsoleCP(CP_UTF8))
	{
		print("FAILURE: Failed to set console codepage to UTF8.");
		return false;
	}

	return true;
}

bool initialize()
{
	// Get the console handles
	console_in = GetStdHandle(STD_INPUT_HANDLE);
	console_out = GetStdHandle(STD_OUTPUT_HANDLE);

	// It's recommended by the Chrome documentation to set the stdin and stdout in binary mode
	if (!setup_console(console_in) || !setup_console(console_out))
	{
		print("FAILURE: Failed to setup the consoles.");
		return EXIT_FAILURE;
	}

	return true;
}

s32 main()
{
	if (!initialize())
	{
		print("FAILURE: Initialization failed.");
		return EXIT_FAILURE;
	}

	std::vector<u8> message;

	if (!receive_message(message))
	{
		print("FAILURE: Receiving the message failed.");
		return EXIT_FAILURE;
	}

	if (message.size() != 4)
	{
		print("FAILURE: Received message's size is wrong. (expected 4, is %d)", message.size());
		return EXIT_FAILURE;
	}

	print("RECEIVED: 0x%X 0x%X 0x%x 0x%X", message[0], message[1], message[2], message[3]);

	if (message[0] != 0x0 || message[1] != 0x1 || message[2] != 0x42 || message[3] != 0xFF)
	{
		print("FAILURE: Received bytes are wrong.");
		return EXIT_FAILURE;
	}

	// Send our own, slightly different, test message
	message[0] = 0x0;
	message[1] = 0x1;
	message[2] = 0x10;
	message[3] = 0xFF;

	if (!send_message(message))
	{
		print("FAILURE: Failed to send test message");
		return EXIT_FAILURE;
	}

	print("SUCCESS! Check the browser to see if the extension counted the test as a success.");
}